The board is supplied with a Marlin firmware. Because of continued development and your specific needs, you may want to upload another firmware. You can use Arduino IDE to upload firmware to the Megatronics board. Under Linux select as board 'Arduino Mega 2560 or Mega ADK'. Under other operating systems the USB will not be reset correctly and you will need to adjust the boards.txt. This file is located in <Arduino directory>/hardware/arduino/boards.txt. Add the following text to the file:

megatronics.name=Megatronics
megatronics.upload.protocol=wiring
megatronics.upload.maximum_size=258048
megatronics.upload.speed=115200
megatronics.bootloader.low_fuses=0xFF
megatronics.bootloader.high_fuses=0xDA
megatronics.bootloader.extended_fuses=0xF5
megatronics.bootloader.path=stk500v2
megatronics.bootloader.file=stk500boot_v2_mega2560.hex
megatronics.bootloader.unlock_bits=0x3F
megatronics.bootloader.lock_bits=0x0F
megatronics.build.mcu=atmega2560
megatronics.build.f_cpu=16000000L
megatronics.build.core=arduino
megatronics.build.variant=mega
