from threading import *
import time
import liblo
from chiplotle import *

try:
    target = liblo.Address(7777)
except liblo.AddressError, err:
    print str(err)
    sys.exit()



class OSCServer(Thread):
	""" Receives OSC stuff from client ( app ) """
	def __init__(self,port=9999):
		Thread.__init__(self)

		self.osc = liblo.Server(port)
		self.osc.add_method(None,None,self.recv)
		
		self.alive = True
		self.callback = lambda s:None

	def set_callback( self, callback ):
		self.callback = callback

	def recv(self, path, args, types, src):
		print "received stuff"
		print(path + " ,")
		print(types)
		# loop through arguments and print them
		for a, t in zip(args, types):
			if t == 's':
				print "calling with " + a
				self.callback(a)
			else:
				# anything else
				print("received uknown message: " + str(a))
				print('\n')

	def run(self):
		while self.alive:
			self.osc.recv(10)
			

chunk_buf = []
verbose = True

plotter = instantiate_plotters( )[0]
p1,p2 = plotter.output_p1p2
print "P1:"+str(p1)+" P2"+str(p2)
max_buffer_space = plotter._buffer_space
print "buffer space: " + str(max_buffer_space)


def osc_str( st ):
	print "received " + st
	chunk_buf.append(st)

def send_status(status):
	""" Send status to client app """
	liblo.send(target, "/status", status)

c_line = [] # length of each line

def wait_for_done():
	""" Wait for buffer in plotter to be empty """
	time.sleep(1)
	bs = plotter._buffer_space

	res = "wait"
	if bs >= max_buffer_space:
		print "buffer is free again..."
		res = "done"

	send_status(res)



def parse_commands(chunk):
	send_status("wait")
	plotter.write(chunk)


def run_server():
	""" Taki server loop"""
	send_status("ok")
	try:
		while 1:
			time.sleep(0.005)
			if not chunk_buf:
				wait_for_done()
			else:
				parse_commands(chunk_buf.pop(0)) # parse next cmd

	except KeyboardInterrupt :
		send_status("waiting")
		osc.alive = False
		osc.join() # waits for thread to finish
		print "quitting, seeya..."



#### OSC SERVER
osc = OSCServer(9999)
osc.set_callback(osc_str)
osc.start()

#### RUN
run_server()


