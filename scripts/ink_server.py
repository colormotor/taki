from threading import *
import time
import serial
import liblo

try:
    target = liblo.Address(7777)
except liblo.AddressError, err:
    print str(err)
    sys.exit()



class OSCServer(Thread):
	""" Receives OSC stuff from client ( app ) """
	def __init__(self,port=9999):
		Thread.__init__(self)

		self.osc = liblo.Server(port)
		self.osc.add_method(None,None,self.recv)
		
		self.alive = True
		self.callback = lambda s:None

	def set_callback( self, callback ):
		self.callback = callback

	def recv(self, path, args, types, src):
		print "received stuff"
		print(path + " ,")
		print(types)
		# loop through arguments and print them
		for a, t in zip(args, types):
			if t == 's':
				print "calling with " + a
				self.callback(a)
			else:
				# anything else
				print("received uknown message: " + str(a))
				print('\n')

	def run(self):
		while self.alive:
			self.osc.recv(10)
			

chunk_buf = []
verbose = True
RX_BUFFER_SIZE = 128

def osc_str( st ):
	print "received " + st
	s.write(st + '\n')

	

def run_server():
	
	try:
		while 1:
			time.sleep(0.005)
			
	except KeyboardInterrupt :
		osc.alive = False
		osc.join() # waits for thread to finish
		print "quitting, seeya..."

#### CONNECT TO SERIAL
try:
	print "attempting serial connection"
	s = serial.Serial('/dev/tty.usbmodem411',115200, timeout=1 )
	print "connected to taki!"
	time.sleep(2)
	s.flushInput()
except serial.SerialException:
	s = None
	print "error! could not connect to taki"

#### OSC SERVER
osc = OSCServer(9999)
osc.set_callback(osc_str)
osc.start()

#### RUN
run_server()


