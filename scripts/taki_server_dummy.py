from threading import *
import time
import serial
import liblo

try:
    target = liblo.Address(7777)
except liblo.AddressError, err:
    print str(err)
    sys.exit()

class OSCServer(Thread):
	""" Receives OSC stuff from client ( app ) """
	def __init__(self,port=9999):
		Thread.__init__(self)

		self.osc = liblo.Server(port)
		self.osc.add_method(None,None,self.recv)
		
		self.alive = True
		self.callback = lambda s:None

	def set_callback( self, callback ):
		self.callback = callback

	def recv(self, path, args, types, src):
		print "received data"
		print(path + " ,")
		print(types)
		# loop through arguments and print them
		for a, t in zip(args, types):
			if t == 's':
				print "calling with " + a
				self.callback(a)
			else:
				# anything else
				print("received uknown message: " + str(a))
				print('\n')

	def run(self):
		while self.alive:
			self.osc.recv(10)
			

chunk_buf = []
verbose = True
RX_BUFFER_SIZE = 128

def osc_str( st ):
	print "received " + st
	chunk_buf.append(st)

def send_status(status):
	""" Send status to client app """
	print "sending status " + status
	liblo.send(target, "/status", status)

c_line = [] # length of each line

def wait_for_done():
	""" Wait for done message from firmware ( plotter is inactive ) """
	stuff = ''
	
	time.sleep(1.0)
	send_status("done")

def parse_commands(chunk):
	""" Parse sequence of commands separated by ; """
	cmds = chunk.split(";")

	send_status("wait")
	
	for line in cmds:
		print line

	#time.sleep(0.01)
	send_status("ok")
	send_status("done")
	return
		
def run_server():
	""" Taki server loop"""
	send_status("ok")

	try:
		while 1:
			#time.sleep(0.001)
			if not chunk_buf:
				wait_for_done()
			else:
				parse_commands(chunk_buf.pop(0)) # parse next cmd

	except KeyboardInterrupt :
		send_status("waiting")
		osc.alive = False
		osc.join() # waits for thread to finish
		print "quitting, seeya..."

#### CONNECT TO SERIAL
print "Taki dummy server, initializing...."

#### OSC SERVER
osc = OSCServer(9999)
osc.set_callback(osc_str)
osc.start()

#### RUN
run_server()


