from threading import *
import time
import serial
import liblo

try:
    target = liblo.Address(7777)
except liblo.AddressError, err:
    print str(err)
    sys.exit()



class OSCServer(Thread):
	""" Receives OSC stuff from client ( app ) """
	def __init__(self,port=9999):
		Thread.__init__(self)

		self.osc = liblo.Server(port)
		self.osc.add_method(None,None,self.recv)
		
		self.alive = True
		self.callback = lambda s:None

	def set_callback( self, callback ):
		self.callback = callback

	def recv(self, path, args, types, src):
		print "received stuff"
		print(path + " ,")
		print(types)
		# loop through arguments and print them
		for a, t in zip(args, types):
			if t == 's':
				print "calling with " + a
				self.callback(a)
			else:
				# anything else
				print("received uknown message: " + str(a))
				print('\n')

	def run(self):
		while self.alive:
			self.osc.recv(10)
			

chunk_buf = []
verbose = True
RX_BUFFER_SIZE = 128

def osc_str( st ):
	print "received " + st
	if st != 'flush':
		chunk_buf.append(st)
	else:
		print "flush cmd (ignored)"

def send_status(status):
	""" Send status to client app """
	liblo.send(target, "/status", status)

c_line = [] # length of each line

def wait_for_done():
	""" Wait for done message from firmware ( plotter is inactive ) """
	stuff = ''
	if not s:
		return

	while s.inWaiting():
		rec = s.readline().strip()
		print "Received: "+rec
		print "sum:"+str(sum(c_line))
		stuff += rec 
		if stuff.find("done") >= 0:
			#print "sending done..."
			send_status("done")
			stuff = '' # crap...
			
		if stuff.find("ok") >= 0 or stuff.find("error") >= 0:
			print "received ok/error"
			if c_line:
				del c_line[0]
				print "now sum is:"+str(sum(c_line))
			else:
				print "Buffer empty"






def parse_commands(chunk):
	""" Parse sequence of commands separated by ; """
	cmds = chunk.split(";")

	send_status("wait")
	
	if not s:
		for line in cmds:
			print line

		time.sleep(0.01)
		send_status("ok")
		send_status("done")
		return

	l_count = 0 # line counter
	g_count = 0 # g code counter

	# Adapted from GRBL G-Code streamer

	'''

	for each command:
		block = line.strip()
		block += ';'

		c_line()
	'''

	for line in cmds:
		l_count += 1 # Iterate line counter
		#     l_block = re.sub('\s|\(.*?\)','',line).upper() # Strip comments/spaces/new line and capitalize
		l_block = line.strip() # remove whitespace
		l_block += ';'

		# add length of block ( command ) to line count buffer
		c_line.append(len(l_block)+2) # Track number of characters in grbl serial read buffer 
		taki_out = '' 
		
		print "parsing line " + l_block
		
		while sum(c_line) >= (RX_BUFFER_SIZE-32) | s.inWaiting() : # if too many characters, and there is stuff to read from serial buffer
			
			print "waiting %d bytes"%sum(c_line)

			out_temp = s.readline().strip() # Wait for taki response
			if out_temp.find('ok') < 0 and out_temp.find('error') < 0:
				print "  Debug: ",out_temp # Debug response  # wait for complete message
			else:
				taki_out += out_temp; # complete message
				g_count += 1 # Iterate g-code counter
				taki_out += str(g_count); # Add line finished indicator
				del c_line[0] # uncount
				print "recv ok/error during loop now total size is ", sum(c_line)
				

		if verbose: print "SND: " + str(l_count) + " : " + l_block,
		s.write(l_block + '\n') # Send block to grbl
		if verbose : print "BUF:",str(sum(c_line)),"REC:",taki_out
		send_status("ok")
		

def run_server():
	""" Taki server loop"""
	send_status("ok")
	try:
		while 1:
			time.sleep(0.005)
			if not chunk_buf:
				wait_for_done()
			else:
				parse_commands(chunk_buf.pop(0)) # parse next cmd
	except KeyboardInterrupt :
		send_status("waiting")
		osc.alive = False
		osc.join() # waits for thread to finish
		print "quitting, seeya..."

#### CONNECT TO SERIAL
try:
	print "attempting serial connection"
	s = serial.Serial('/dev/tty.usbserial-AM01Y0IC',115200, timeout=1 )
	print "connected to taki!"
	time.sleep(2)
	#welcome = s.readline()
	#print welcome
	#s.flushInput()
except serial.SerialException:
	s = None
	print "error! could not connect to taki"

#### OSC SERVER
osc = OSCServer(9999)
osc.set_callback(osc_str)
osc.start()

#### RUN
run_server()


