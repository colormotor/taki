#include "Marlin.h"

//#include "ultralcd.h"
#include "planner.h"
#include "stepper.h"
#include "motion_control.h"
//#include "cardreader.h"
//#include "watchdog.h"
//#include "ConfigurationStore.h"
#include "language.h"
#include "pins.h"
#include "ConfigurationStore.h"
#include "taki_cmds.h"
#include "taki_interpreter.h"


#define VERSION_STRING  "alpha"

//------------------------------------------------
// Init 
//------------------------------------------------
int taki_initialize()
{ 
  MYSERIAL.begin(BAUDRATE);
  SERIAL_PROTOCOLLNPGM("start");
  SERIAL_ECHO_START;

  SERIAL_PROTOCOL(ASCII_XON);
  
  SERIAL_ECHOPGM(MSG_MARLIN);
  SERIAL_ECHOLNPGM(VERSION_STRING);

  SERIAL_ECHOPGM(MSG_FREE_MEMORY);
  SERIAL_ECHO(free_memory());

  SERIAL_ECHOPGM(MSG_PLANNER_BUFFER_BYTES);
  SERIAL_ECHOLN((int)sizeof(block_t)*BLOCK_BUFFER_SIZE);


  Config_RetrieveSettings();  // loads data from EEPROM if available

  delay(100);

  for(int8_t i=0; i < NUM_AXIS; i++)
  {
    axis_steps_per_sqr_second[i] = max_acceleration_units_per_sq_second[i] * axis_steps_per_unit[i];
  }

  // initialize pen up pen down mechanism
  init_pen();
  // initialize ink flowing mechanism
  init_ink();
  plan_init();  // Initialize planner;
  //watchdog_init();
  st_init();    // Initialize stepper, this enables interrupts!

  // tell whovever sends stuff we are ready...
  

  go_home();

  delay(1000);
  send_status_message(1);
  //setup_photpin();

}


void kill()
{
  cli(); // Stop interrupts

  disable_x();
  disable_y();
  disable_z();
  disable_e0();
  disable_e1();
  disable_e2();
  
  SERIAL_ECHO("Killing.....");
 // suicide();
  while(1) { /* Intentionally left empty */ } // Wait for reset
}

static unsigned long previous_millis_cmd = 0;
static unsigned long max_inactive_time = 0;
static unsigned long stepper_inactive_time = DEFAULT_STEPPER_DEACTIVE_TIME*1000l;

extern bool ink_flowing;
extern unsigned long previous_ink_flow;
extern unsigned long ink_flow_time;

unsigned long done_cmd_millis = 0;
unsigned long prev_cmd_millis = 0;

void manage_inactivity() 
{ 
  /*
  if( (millis() - previous_millis_cmd) >  max_inactive_time ) 
    if(max_inactive_time) 
      kill(); 
  if(stepper_inactive_time)  {
    if( (millis() - previous_millis_cmd) >  stepper_inactive_time ) 
    {
      if(blocks_queued() == false) {
        disable_x();
        disable_y();
        disable_z();
        disable_e0();
        disable_e1();
        disable_e2();
      }
    }
  }
  #if( KILL_PIN>-1 )
    if( 0 == READ(KILL_PIN) )
      kill();
  #endif
  #ifdef CONTROLLERFAN_PIN
    controllerFan(); //Check if fan should be turned on to cool stepper drivers down
  #endif*/
  
  if( ink_flowing && ((millis() - previous_ink_flow) > ink_flow_time ) )
  {
    ink_off();
  }

  check_axes_activity();
}


int taki_loop()
{
  /*
	if(buflen < (BUFSIZE-1))
    get_command();
  
  if(buflen)
  {
#ifdef SDSUPPORT
    if(savetosd)
    {
        if(strstr(cmdbuffer[bufindr],"M29") == NULL)
        {
            write_command(cmdbuffer[bufindr]);
            showString(PSTR("ok\r\n"));
        }
        else
        {
            file.sync();
            file.close();
            savetosd = false;
            showString(PSTR("Done saving file.\r\n"));
        }
    }
    else
    {
        process_commands();
    }
#else
    process_commands();
#endif

    buflen = (buflen-1);
    //bufindr = (bufindr + 1)%BUFSIZE;
    //Removed modulo (%) operator, which uses an expensive divide and multiplication
    bufindr++;
    if(bufindr == BUFSIZE) bufindr = 0;
  }
  */
 // process_commands();
  //check heater every n milliseconds
 // manage_inactivity(1);

 /* for( int i = 0; i < 5; i++ )
  {
    serial_printf("%d\n",i);
    set_speed(1.0);
    moveto(rand()%500,rand()%500 );
    st_synchronize();
  }
*/

  read_commands();
  manage_inactivity();

  unsigned long mil = millis();
  done_cmd_millis += (mil-prev_cmd_millis);
  prev_cmd_millis = mil;

  if(done_cmd_millis > 1000)
  {
      if(movesplanned()==0)
      {
        SERIAL_PROTOCOL("done\n");
      }else{
        //SERIAL_PROTOCOL("plotting\n");
      }
      done_cmd_millis = 0;
  }
  //lcd_update();
}
