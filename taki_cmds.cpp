

#include "taki_cmds.h"
#include "Marlin.h"
#include "stepper.h"
#include "MarlinServo.h"

static float speed = 0.5*10*10; //1.0;
static float accel = 1.0;
static int fd = 10*10; // HACK gotta figure this out....
//static float px;
//static float py;

Servo penServo;
Servo inkServo;

bool currently_homing = false;

bool ink_flowing = false;
unsigned long previous_ink_flow = 0;
unsigned long ink_flow_time = 1000; // one sec

static int penstate = 0;

float homing_feedrate[] = HOMING_FEEDRATE;
float current_position[2] = { 0.0, 0.0 };
float add_homeing[2]={0,0};
float min_pos[2] = { X_MIN_POS, Y_MIN_POS };
float max_pos[2] = { X_MAX_POS, Y_MAX_POS };

const char axis_codes[2] = {'X', 'Y' };
static float destination[2] = {  0.0, 0.0 };
static float offset[2] = {0.0, 0.0};


// clamps position to limits
void clamp_to_software_endstops(float target[2])
{
  if (min_software_endstops) {
    if (target[X_AXIS] < min_pos[X_AXIS]) target[X_AXIS] = min_pos[X_AXIS];
    if (target[Y_AXIS] < min_pos[Y_AXIS]) target[Y_AXIS] = min_pos[Y_AXIS];
  }

  if (max_software_endstops) {
    if (target[X_AXIS] > max_pos[X_AXIS]) target[X_AXIS] = max_pos[X_AXIS];
    if (target[Y_AXIS] > max_pos[Y_AXIS]) target[Y_AXIS] = max_pos[Y_AXIS];
  }
}


// test cmds:
// PD;TI;PA122,322;PA322,122;PA432,123;PU;PA223,122;PA123,444;PD;TI;PA0,0;PA30,60;
void init_ink()
{
	inkServo.attach(INK_SERVO_PIN);
	ink_off();
}


void init_pen()
{
	penServo.attach(PEN_SERVO_PIN);
	penchange();
}

void set_speed( float val )
{
	speed = val;
}

void set_accel( float val )
{
	acceleration = val;
}

void set_jerk( float val )
{
	max_xy_jerk = val;
}

void get_pos(float*x,float*y)
{
	*x = current_position[0];
	*y = current_position[1];
}

void moveto( float x, float y )
{
	destination[0] = x;
	destination[1] = y;
	clamp_to_software_endstops(destination);
	
	//SERIAL_ECHO("planning line...");
	plan_buffer_line(destination[0],destination[1],1,1, speed,0 );

	current_position[0] = destination[0];
	current_position[1] = destination[1];
}

void move( float x, float y )
{
	moveto(current_position[0]+x,current_position[1]+y);
}

void togglepen()
{
	if(penstate)
		penup();
	else
	{
		pendown();
		ink_on();
	}
}

void penup()
{
	// Something weird is going on with the servo angles here :/...
	//SERIAL_ECHO("penup");
	penServo.write(130); //65);
	delay(500);
	penstate = 0;
	//
}

void penangle(int ang)
{
	penServo.write(ang);
}

void penchange()
{
	penServo.write(110); //150);
}

void pendown()
{
	penServo.write(150); //180);
	delay(500);
	penstate = 1;
}


void set_ink_time( long val )
{
	ink_flow_time = val;
}


void ink_on()
{
	previous_ink_flow = millis();
	ink_flowing = true;
	inkServo.write(180);
}

void ink_off()
{
	ink_flowing = false;
	//inkServo.write(110);
}

#define DEFINE_PGM_READ_ANY(type, reader)		\
    static inline type pgm_read_any(const type *p)	\
	{ return pgm_read_##reader##_near(p); }

DEFINE_PGM_READ_ANY(float,       float);
DEFINE_PGM_READ_ANY(signed char, byte);

#define XYZ_CONSTS_FROM_CONFIG(type, array, CONFIG)	\
static const PROGMEM type array##_P[3] =		\
    { X_##CONFIG, Y_##CONFIG, Z_##CONFIG };		\
static inline type array(int axis)			\
    { return pgm_read_any(&array##_P[axis]); }


#define XYZ_CONSTS_FROM_CONFIG(type, array, CONFIG)	\
static const PROGMEM type array##_P[3] =		\
    { X_##CONFIG, Y_##CONFIG, Z_##CONFIG };		\
static inline type array(int axis)			\
    { return pgm_read_any(&array##_P[axis]); }

XYZ_CONSTS_FROM_CONFIG(float, base_min_pos,    MIN_POS);
XYZ_CONSTS_FROM_CONFIG(float, base_max_pos,    MAX_POS);
XYZ_CONSTS_FROM_CONFIG(float, base_home_pos,   HOME_POS);
XYZ_CONSTS_FROM_CONFIG(float, max_length,      MAX_LENGTH);
XYZ_CONSTS_FROM_CONFIG(float, home_retract_mm, HOME_RETRACT_MM);
XYZ_CONSTS_FROM_CONFIG(signed char, home_dir,  HOME_DIR);


float feedrate = 0;

static void axis_is_at_home(int axis) {
  current_position[axis] = base_home_pos(axis) + add_homeing[axis];
  min_pos[axis] =          base_min_pos(axis) + add_homeing[axis];
  max_pos[axis] =          base_max_pos(axis) + add_homeing[axis];
}

void serial_printf(char *fmt, ... );

static void homeaxis(int axis) {



#define HOMEAXIS_DO(LETTER) \
  ((LETTER##_MIN_PIN > -1 && LETTER##_HOME_DIR==-1) || (LETTER##_MAX_PIN > -1 && LETTER##_HOME_DIR==1))

  SERIAL_PROTOCOL("homing axis \n");

  if (axis==X_AXIS ? HOMEAXIS_DO(X) :
      axis==Y_AXIS ? HOMEAXIS_DO(Y) :
      axis==Z_AXIS ? HOMEAXIS_DO(Z) :
      0) {
    current_position[axis] = 0;
    plan_set_position(current_position[X_AXIS], current_position[Y_AXIS], 1,1);
    destination[axis] = 1.5 * max_length(axis) * home_dir(axis);
    feedrate = homing_feedrate[axis];
    plan_buffer_line(destination[X_AXIS], destination[Y_AXIS], 1,1, feedrate/60, 0);
    st_synchronize();
   	
    current_position[axis] = 0;
    plan_set_position(current_position[X_AXIS], current_position[Y_AXIS],  1,1 );
    destination[axis] = -home_retract_mm(axis) * home_dir(axis);
    plan_buffer_line(destination[X_AXIS], destination[Y_AXIS],  1,1, feedrate/60, 0);
    st_synchronize();
   
    destination[axis] = 2*home_retract_mm(axis) * home_dir(axis);
    feedrate = homing_feedrate[axis]/2 ; 
    plan_buffer_line(destination[X_AXIS], destination[Y_AXIS],  1,1, feedrate/60, 0);
    st_synchronize();
   
    axis_is_at_home(axis);					
    destination[axis] = current_position[axis];
    feedrate = 0.0;
    endstops_hit_on_purpose();
  }
}
#define HOMEAXIS(LETTER) homeaxis(LETTER##_AXIS)


void go_home()
{
	currently_homing = true;
	float saved_feedrate = feedrate;
	//previous_millis_cmd = millis();

	enable_endstops(true);

	for(int8_t i=0; i < 2; i++) {
		destination[i] = current_position[i];
	}

	feedrate = 0.0;

	// diagonal homing move
	current_position[X_AXIS] = 0;current_position[Y_AXIS] = 0;  
	destination[X_AXIS] = 1.5 * X_MAX_LENGTH * X_HOME_DIR;
	destination[Y_AXIS] = 1.5 * Y_MAX_LENGTH * Y_HOME_DIR;  

	feedrate = homing_feedrate[X_AXIS]; 
    if(homing_feedrate[Y_AXIS]<feedrate)
      feedrate =homing_feedrate[Y_AXIS]; 

	
    
    plan_set_position(current_position[X_AXIS], current_position[Y_AXIS], 1,1 ); 
    destination[X_AXIS] = 1.5 * X_MAX_LENGTH * X_HOME_DIR; destination[Y_AXIS] = 1.5 * Y_MAX_LENGTH * Y_HOME_DIR;  
    
    plan_buffer_line(destination[X_AXIS], destination[Y_AXIS], 1,1 , feedrate/60, 9);
    st_synchronize();

    axis_is_at_home(X_AXIS);
  

    axis_is_at_home(Y_AXIS);
    plan_set_position(current_position[X_AXIS], current_position[Y_AXIS], 1,1 ); 
    destination[X_AXIS] = current_position[X_AXIS];
    destination[Y_AXIS] = current_position[Y_AXIS];
    plan_buffer_line(destination[X_AXIS], destination[Y_AXIS], 1,1 , feedrate/60, 9);
    feedrate = 0.0;
    st_synchronize();
    endstops_hit_on_purpose();
    
    // adjust pos
	HOMEAXIS(X);
	HOMEAXIS(Y);
	
	plan_set_position(current_position[X_AXIS], current_position[Y_AXIS], 1,1 );

	#ifdef ENDSTOPS_ONLY_FOR_HOMING
	enable_endstops(false);
	#endif

	//feedrate = saved_feedrate;
	//feedmultiply = saved_feedmultiply;
	//previous_millis_cmd = millis();

	// sets endstops to false.....
	endstops_hit_on_purpose();

	currently_homing = false;
}

void refill()
{
	// refilll
}
