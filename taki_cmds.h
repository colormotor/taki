#ifndef _takicmdsh_
#define _takicmdsh_

void init_pen();
void init_ink();

void go_home();

void get_pos(float*x,float*y);

void set_speed( float val );
void set_accel( float val );
void set_jerk( float val );

void moveto( float x, float y );
void move( float dx, float dy );

void penangle(int ang);
void penup();
void pendown();
void penchange();
void togglepen();

void set_ink_time( long val );
void ink_on();
void ink_off();

void go_home();
void refill();

#endif

