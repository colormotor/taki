

#include "taki_cmds.h"
#include "Marlin.h"
#include "stepper.h"
#include "MarlinServo.h"

static float speed = 1.0;
static int fd = 10*10; // HACK gotta figure this out....
static float px;
static float py;

Servo penServo;
Servo inkServo;

bool ink_flowing = false;
unsigned long previous_ink_flow = 0;
unsigned long ink_flow_time = 1000; // one sec

static int penstate = 0;

// test cmds:
// PD;TI;PA122,322;PA322,122;PA432,123;PU;PA223,122;PA123,444;PD;TI;PA0,0;PA30,60;
void init_ink()
{
	inkServo.attach(INK_SERVO_PIN);
	ink_off();
}

void init_pen()
{
	penServo.attach(PEN_SERVO_PIN);
	penup();
}

void set_speed( float val )
{
	speed = val;
}

void get_pos(float*x,float*y)
{
	*x = px;
	*y = py;
}

void moveto( float x, float y )
{
	px = x;
	py = y;
	SERIAL_PROTOCOL("moving to");

    SERIAL_PROTOCOL(x);
    SERIAL_PROTOCOL(", ");
	SERIAL_PROTOCOL(y);
	SERIAL_PROTOCOL("\n");
	//SERIAL_ECHO("planning line...");
	plan_buffer_line(x,y,1,1, speed*fd,0 );
}

void move( float x, float y )
{
	moveto(px+x,py+y);
}

void togglepen()
{
	if(penstate)
		penup();
	else
	{
		pendown();
		ink_on();
	}
}

void penup()
{
	// Something weird is going on with the servo angles here :/...
	//SERIAL_ECHO("penup");
	penServo.write(30);
	delay(300);
	penstate = 0;
	//
}

void penangle(int ang)
{
	penServo.write(ang);
}

void pendown()
{
	penServo.write(45);
	delay(300);
	penstate = 1;
}


void set_ink_time( long val )
{
	ink_flow_time = val;
}


void ink_on()
{
	previous_ink_flow = millis();
	ink_flowing = true;
	inkServo.write(180);
	SERIAL_PROTOCOL("Ink on...\n");
}

void ink_off()
{
	ink_flowing = false;
	inkServo.write(110);
}

void go_home()
{
	// homing
}

void refill()
{
	// refilll
}
