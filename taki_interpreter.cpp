
#include <avr/pgmspace.h>
#include <math.h>

#include "Marlin.h"
#include "stepper.h"
#include "planner.h"
#include "taki_interpreter.h"
#include "taki_cmds.h"

void serial_printf(char *fmt, ... )
{
        char tmp[128]; // resulting string limited to 128 chars
        va_list args;
        va_start (args, fmt );
        vsnprintf(tmp, 128, fmt, args);
        va_end (args);
        SERIAL_PROTOCOL(tmp);
}


extern "C"{
  extern unsigned int __bss_end;
  extern unsigned int __heap_start;
  extern void *__brkval;

  int free_memory() {
    int free_memory;

    if((int)__brkval == 0)
      free_memory = ((int)&free_memory) - ((int)&__bss_end);
    else
      free_memory = ((int)&free_memory) - ((int)__brkval);

    return free_memory;
  }
}


// comm variables and Commandbuffer
// BUFSIZE is reduced from 8 to 6 to free more RAM for the PLANNER
#define MAX_CMD_SIZE 96
#define MAX_CMDS 10
#define BUFSIZE 6 //8

static char cmd[MAX_CMD_SIZE+1];
int cmdLen = 0;




int   parse_floats(float vals[], int n, const char * str, char separator = ',' )
{
  char token[64];

  char * c = (char*)str;
  int ind = 0;
  while( *c != '\0' && *c != ';' && ind < n )
  {
    int i = 0;
    
    if(*c==separator || *c==' ')
    {
      c++;
      continue;
    }

    while( *c!=separator && *c!='\0' && *c!=' ' && *c!='\t' && *c!=';' )
    {
      token[i++] = *c++;
    }
    token[i] = '\0';
    vals[ind] = atof(token);
    ind++;
  }

  return ind;
}


int   parse_ints(int vals[], int n, const char * str, char separator = ',' )
{
  char token[64];

  char * c = (char*)str;
  int ind = 0;
  while( *c != '\0' && *c != ';' && ind < n )
  {
    int i = 0;

    while( *c==separator || (*c==' ') )
      c++;

    while( *c!=separator && *c!='\0' && *c!=';' )
    {
      if(*c == ' ' )
        break;
      token[i++] = *c++;
    }
    token[i] = '\0';
    vals[ind] = atoi(token);
    ind++;
  }

  return ind;
}

int parse_command();
void parse_escape( char c );


void add_cmd_char( char c)
{
  cmd[cmdLen++] = c;
  cmd[cmdLen] = '\0';
  //serial_printf("cmd is now %s len %d\n",cmd,cmdLen);
}

void reset_cmd()
{
  cmd[0] = '\0';
  cmdLen = 0;
}

#define ESC 0x1B


bool xoff = false;
int max_serial = 50;
int ok_serial = 5;

/*
void send_xon()
{
  SERIAL_PROTOCOL_FLUSH("XON");
  SERIAL_PROTOCOL_FLUSH(ASCII_XON);
  xoff = false;
}

void send_xoff()
{
  xoff = true;
  SERIAL_PROTOCOL_FLUSH("XOFF");
  SERIAL_PROTOCOL_FLUSH(ASCII_XOFF);
}
*/

/// Send status message to server GRBL style
void send_status_message( int ok )
{
  if(ok)
  {
    SERIAL_PROTOCOLLNPGM("ok\n");
  }
  else
  {
    SERIAL_PROTOCOLLNPGM("error\n");
  }
}

/*
void protocol_process()
{
  uint8_t c;
  while((c = serial_read()) != SERIAL_NO_DATA) {
    if ((c == '\n') || (c == '\r')) { // End of line reached

      // Runtime command check point before executing line. Prevent any furthur line executions.
      // NOTE: If there is no line, this function should quickly return to the main program when
      // the buffer empties of non-executable data.
      protocol_execute_runtime();
      if (sys.abort) { return; } // Bail to main program upon system abort    

      if (char_counter > 0) {// Line is complete. Then execute!
        line[char_counter] = 0; // Terminate string
        report_status_message(protocol_execute_line(line));
      } else { 
        // Empty or comment line. Skip block.
        report_status_message(STATUS_OK); // Send status message for syncing purposes.
      }
      char_counter = 0; // Reset line buffer index
      iscomment = false; // Reset comment flag
      
    } else {
      if (iscomment) {
        // Throw away all comment characters
        if (c == ')') {
          // End of comment. Resume line.
          iscomment = false;
        }
      } else {
        if (c <= ' ') { 
          // Throw away whitepace and control characters
        } else if (c == '/') { 
          // Block delete not supported. Ignore character.
        } else if (c == '(') {
          // Enable comments flag and ignore all characters until ')' or EOL.
          iscomment = true;
        } else if (char_counter >= LINE_BUFFER_SIZE-1) {
          // Throw away any characters beyond the end of the line buffer          
        } else if (c >= 'a' && c <= 'z') { // Upcase lowercase
          line[char_counter++] = c-'a'+'A';
        } else {
          line[char_counter++] = c;
        }
      }
    }
  }
}
*/

void read_commands()
{
  //cmdLen = 0;
  bool bEsc = false;

  while(  MYSERIAL.available() ) 
  {
    char c = MYSERIAL.read();

    if( cmdLen >= MAX_CMD_SIZE )
    {
      SERIAL_PROTOCOLLN("trunkating");
      add_cmd_char(';');
      int res = parse_command();
      reset_cmd();
      send_status_message(res);
      return;
    }

    if(c==';')
    {
      // end of command
      add_cmd_char(c);
      int res = parse_command();
      reset_cmd();
      send_status_message(res);
    }
    else
    {
       if (c <= ' ')
       {
        // skip control chars
       }
       else
       {
          add_cmd_char(c);
       }
    }
    
    //delay (10); 
  }

}

void parse_escape( char c )
{
  switch(c)
  {
    case 'B':
      serial_printf("%d\n",free_memory()/1024);
      break;
  }
}
int parse_command()
{
  float vals[2];

  //serial_printf("taki parsing %s\n",cmd);

  // pen movement commands
  if( cmd[0] == 'P')
  {
    
    int n = 0;
    switch(cmd[1])
    {
      case 'H':
        // home
        go_home();
        SERIAL_PROTOCOLLN("home..\n");
        break;

      case 'A':
        // absolute
        n = parse_floats(vals,2,cmd+2);
        if( n == 2 )
        {
          moveto(vals[0],vals[1]);
        }
        else
        {
          //serial_printf("%s wrong numner of arguments %d for PA",cmd,n);
          return 0;
        }
        break;

      case 'R':
        // relative
        if( parse_floats(vals,2,cmd+2) == 2 )
        {
          move(vals[0],vals[1]);
        }
        else
        {
          //serial_printf("wrong numner of arguments for PR");
          return 0;
        }
        break;

      case 'U':
        st_synchronize(); // wait for movement to finish
        //SERIAL_PROTOCOLLN("penup");
        penup();
        break;

      case 'D':
        st_synchronize(); // wait for movement to finish
        //SERIAL_PROTOCOLLN("pendown");
        pendown();
        break;
        
    }
  }
  // velocity etc
  else if( cmd[0] == 'V')
  {
    switch(cmd[1])
    {
      case 'S':
        if( parse_floats(vals,1,cmd+2) == 1 )
        {
          set_speed(vals[0]);
          SERIAL_ECHO("vs");
          SERIAL_ECHOLN(vals[0]);
        }
        else
        {
          //serial_printf("wrong numner of arguments for VS");
          return 0;
        }
        break;
      case 'A':
        if( parse_floats(vals,1,cmd+2) == 1 )
        {
          set_accel(vals[0]);
         SERIAL_ECHO("va");
          SERIAL_ECHOLN(vals[0]);
        }
        else
        {
          //serial_printf("wrong numner of arguments for VS");
          return 0;
        }
        break;
      case 'J':
        if( parse_floats(vals,1,cmd+2) == 1 )
        {
          set_jerk(vals[0]);
          SERIAL_ECHO("vj");
          SERIAL_ECHOLN(vals[0]);
        }
        else
        {
          //serial_printf("wrong numner of arguments for VS");
          return 0;
        }
        break;
    }
  }
  // taki specific cmds
  else if( cmd[0] == 'T' )
  {
    switch(cmd[1])
    {
      case 'R':
        // random move ( for debugging )
        if( parse_floats(vals,2,cmd+2) == 2 )
        {
          for( int i = 0; i < vals[0]; i++ )
          {
            if((rand()%100)>50)
            {
              st_synchronize();
              togglepen();
            }

            moveto(rand()%(int)vals[1],rand()%(int)vals[1]);
          }
        }
        else
        {
          //serial_printf("wrong numner of arguments for TR");
          return 0;
        }
        break;

  
      case 'P':
        // random move ( for debugging )
        if( parse_floats(vals,2,cmd+2) == 2 )
        {
          for( int i = 0; i < vals[0]; i++ )
          {
            
            if((rand()%100)>50)
            {
              st_synchronize();
              togglepen();
            }
            
            moveto(rand()%(int)vals[1],rand()%(int)vals[1]);

          }
        }
        else
        {
          serial_printf("wrong numner of arguments for TP");
          return 0;
        }
        break;

      case 'I':
        // shoot a burst of ink
        ink_on();
        break;

      case 'T':
        if( parse_floats(vals,1,cmd+2) == 1 )
        {
          //serial_printf("Ink time is now %d\n",(int)vals[0]);
          set_ink_time(vals[0]);
        }
        else
        {
          serial_printf("wrong numner of arguments for TT");
          return 0;
        }
        break;

      case 'A':
        if( parse_floats(vals,1,cmd+2) == 1 )
        {
          //serial_printf("angle is now %d\n",(int)vals[0]);
          penangle(vals[0]);
        }
        else
        {
          //serial_printf("wrong numner of arguments for TT");
          return 0;
        }
        break;

      case 'C':
        // pen change
        penchange();
        break;

    }

  }
  // Request output
  else if( cmd[0] == 'O' )
  {
    float x,y;

    switch(cmd[1])
    {
      case 'A':
        // actual position
        {
          st_get_actual_position(&x,&y);
          //serial_printf("%d,%d\n",(int)x,(int)y);
        }
        break;

      case 'C':
        {
          get_pos(&x,&y);
          //serial_printf("%d,%d\n",(int)x,(int)y);
        }
        break;

      case 'P':
      // p1 and p2
        break;

      case 'I':
        //serial_printf("Taki firmware alpha\n");
        break;
    }
  }
  else if(cmd[0]==ESC)
  {
    parse_escape(cmd[1]);
  }
  else
  {
    //serial_printf("Could not parse cmd: %s\n",cmd);
    return 0;
  }

  return 1;
}

// TODO homing.. end switches ( later? )
