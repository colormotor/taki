#ifndef _interpreterh_
#define _interpreterh_


/// ascii XOFF character
#define		ASCII_XOFF	19
/// ascii XON character
#define		ASCII_XON		17

void send_xon();
void send_xoff();
void send_status_message( int ok );

int free_ram();
void serial_printf(char *fmt, ... );

void read_commands();

#endif
